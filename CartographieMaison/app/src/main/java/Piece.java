/**
 * Created by Jordan on 01/12/2016.
 */

public class Piece extends Plan{

    float largeur;
    float longueur;
    String nom;

    //La position du point représente le point haut gauche
    float x;
    float y;

    public Piece(){
        this.x = 0;
        this.y = 0;

        this.largeur = 10;
        this.longueur = 10;

        this.nom = "null";
    }

    public void modifierPiece(float largeur, float longueur){
        this.largeur = largeur;
        this.longueur = longueur;
    }

    public void deplacerPiece(float x, float y){
        this.x = x;
        this.y = y;
    }

    public void DessinerPiece(){}


}
