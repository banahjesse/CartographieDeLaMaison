/**
 * Created by Jordan on 01/12/2016.
 */
import java.util.*;


public class Plan {

    //Attributs
    ArrayList<Piece> listPiece;
    String nom;

    public Plan(){
        listPiece = new ArrayList<Piece>();
    }

    public void AjouterPiece(Piece piece){
        listPiece.add(piece);
    }

    public void SupprimerPiece(Piece piece){
        listPiece.remove(piece);
    }

    public void collerPiece(Piece piece){
        //Algo pour coller une pièce au plus proche mur
    }

}
