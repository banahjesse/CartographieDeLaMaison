// Simple class example

function SimpleSquareParticle(posX, posY) {
		this.x = posX;
		this.y = posY;
		this.velX = 0;
		this.velY = 0;
		this.accelX = 0;
		this.accelY = 0;
		this.color = "#000000";
		this.radius = 10;
		this.width = 100;
		this.height = 100;
		this.nom = "rien";
}

//The function below returns a Boolean value representing whether the point with the coordinates supplied "hits" the particle.
SimpleSquareParticle.prototype.hitTest = function(hitX,hitY) {
	return((hitX > this.x - this.width)&&(hitX < this.x + this.width)&&(hitY > this.y - this.height)&&(hitY < this.y + this.height));
	//return((hitX > this.x )&&(hitX < this.x + this.width)&&(hitY > this.y)&&(hitY < this.y + this.height));
}

//A function for drawing the particle.
SimpleSquareParticle.prototype.drawToContext = function(theContext) {
	theContext.fillStyle = this.color;
	theContext.strokeRect(this.x - this.width, this.y - this.height, this.width, this.height);
	theContext.fillText(this.nom,this.x - this.width + this.width/3 ,this.y - this.height + this.height/2 );
	theContext.fillText(this.height,this.x - this.width - 10 ,this.y - this.height + 10 ); // Affichage de la longueur
	theContext.fillText(this.width,this.x - this.width + 10 ,this.y - this.height - 15 ); // Affichage de la longueur
}